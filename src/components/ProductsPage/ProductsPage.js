import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Spinner } from '../Loader/Spinner';
import { ProductPage } from '../ProductPage/ProductPage';
import './ProductsPage.css';

export const ProductsPage = () => {
  const { productId } = useParams();
  const [state, setState] = useState({});
  useEffect(() => {
    fetch(`https://fakestoreapi.com/products/${productId}`)
      .then((res) => {
        if (!res.ok) {
          throw new Error(res.statusText);
        }
        return res.json();
      })
      .catch((err) => {
        console.log(err);
      })
      .then((res) => {
        setState({ products: res });
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderUsers = () => {
    if (state.products) {
      return (
        <ProductPage
          id={state.products.id}
          title={state.products.title}
          price={state.products.price}
          image={state.products.image}
          description={state.products.description}
          category={state.products.category}
        />
      );
    }

    return <Spinner />;
  };

  return <div className='wrapper'>{renderUsers()}</div>;
};
