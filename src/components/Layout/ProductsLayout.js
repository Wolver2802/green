import { Outlet } from 'react-router';
import './Layout.css';

function ProductsLayout() {
  return (
    <div className='main__wrapper'>
      <Outlet />
    </div>
  );
}

export default ProductsLayout;
